# prime_number_generator

```
function isPrime(i) {
  if (i === 1) {
    return false
  }
  if (i % 2 === 0 && i !== 2) {
    return false
  }
  if (i % 3 ===0 && i !== 3) {
    return false
  }
  if (i % 5 ===0 && i !== 5) {
    return false
  }
  if (i % 7 ===0 && i !== 7) {
    return false
  }

  return true

}

let primeNumber = function* () {
  let i = 1
  while(true) {
    if(isPrime(i)) {
        yield i
    }
    i++
  }
}


let iterator = primeNumber()
console.log(iterator.next().value) //2
console.log(iterator.next().value) //3
console.log(iterator.next().value) //5
console.log(iterator.next().value) //7
...
```